package com.jiten.innovations.springCloud.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.jiten.innovations.springCloud.configuration.RibbonConfig;

@RibbonClient(name="tollRateServices",configuration = RibbonConfig.class)
@RestController
public class RibbonHardCodedListOfServersController {
	
	@LoadBalanced
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/tollRate-RibbonDemo/{stationId}")
	public String getTollRate(@PathVariable int stationId) {
		Map paramsMap = new HashMap();
		paramsMap.put("stationId",stationId);
		Object forObject = restTemplate.getForObject("http://tollRateServices/tollRate/{stationId}", String.class,paramsMap);
		return forObject.toString();
	}
	
}
