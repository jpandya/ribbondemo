package com.jiten.innovations.springCloud.ribbonDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.jiten.innovations.springCloud.controller"})
public class RibbonDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RibbonDemoApplication.class, args);
	}

}
